1. cd into the project folder
2. docker-compose up -d
3. To set up project
   1. docker exec -it edclass-symfony-app bash
   2. sh setup.sh
4. To check phpcs, phpstan, phpunit:
   1. docker exec -it edclass-symfony-app bash
   2. To check phpcs: ./vendor/bin/phpcs
   3. To check phpstan: ./vendor/bin/phpstan analyze
   4. To check phpunit: ./vendor/bin/simple-phpunit
5. Import Student.postman_collection.json to Postman and starting testing
