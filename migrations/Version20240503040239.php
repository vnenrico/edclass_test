<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Types\Types;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240503040239 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        if (! $schema->hasSequence('key_stage_id_seq')) {
            $schema->createSequence('key_stage_id_seq');
        }

        if (! $schema->hasSequence('student_id_seq')) {
            $schema->createSequence('student_id_seq');
        }

        $table_key_stage = $schema->createTable('key_stage');
        $table_key_stage->addColumn('id', Types::INTEGER)->setAutoincrement(true);
        $table_key_stage->addColumn('name', Types::STRING)->setLength(5);
        $table_key_stage->setPrimaryKey(['id']);

        $table_student = $schema->createTable('student');
        $table_student->addColumn('id', Types::INTEGER)->setAutoincrement(true);
        $table_student->addColumn('firstname', Types::STRING)->setLength(255);
        $table_student->addColumn('lastname', Types::STRING)->setLength(255);
        $table_student->addColumn('birthday', Types::DATE_MUTABLE);
        $table_student->addColumn('created_at', Types::DATETIME_IMMUTABLE)->setDefault('CURRENT_TIMESTAMP');
        $table_student->addColumn('updated_at', Types::DATETIME_IMMUTABLE)->setNotnull(false);
        $table_student->addColumn('key_stage_id', Types::INTEGER);
        $table_student->setPrimaryKey(['id']);

        $table_student->addForeignKeyConstraint($table_key_stage, ['key_stage_id'], ['id'], [], 'student_fk_key_stage');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        if (! $schema->hasSequence('public')) {
            $schema->createSequence('public');
        }

        $schema->dropSequence('key_stage_id_seq');
        $schema->dropSequence('student_id_seq');
        $schema->dropTable('key_stage');
        $schema->dropTable('student');
    }
}
