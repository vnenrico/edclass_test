# Use an official PHP runtime as a parent image
FROM php:8.3-apache

# Set the working directory in the container
WORKDIR /var/www/html

# Install PostgreSQL client and other dependencies
RUN apt-get update && apt-get install -y \
    postgresql-client \
    libpq-dev \
    && docker-php-ext-install pdo pdo_pgsql

# Copy composer.lock and composer.json
COPY composer.lock composer.json ./

# Install Symfony
RUN apt-get update && apt-get install -y \
    curl \
    git \
    unzip \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Install Symfony CLI (optional)
RUN curl -sS https://get.symfony.com/cli/installer | bash

COPY docker_resources/apache2/000-default.conf /etc/apache2/sites-enabled/000-default.conf

# Copy the current directory contents into the container at /var/www/html
COPY . .

# Expose port 80 to the outside world
EXPOSE 80

# Start Apache server
CMD ["apache2-foreground"]
