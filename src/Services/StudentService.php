<?php

namespace App\Services;

use App\DataTransferObject\StudentDTO;
use App\Entity\KeyStage;
use App\Entity\Student;
use App\Repository\KeyStageRepository;
use App\Repository\StudentRepository;
use DateTime;
use Exception;

readonly class StudentService
{
    /**
     * @param array<int, array<string, int>> $ageKeyStageMapping
     */
    public function __construct(
        private array $ageKeyStageMapping,
        private StudentRepository $studentRepo,
        private KeyStageRepository $keyStageRepo
    ) {
    }

    /**
     * @throws Exception
     */
    public function createStudent(StudentDTO $dto): Student
    {
        $student = new Student();
        $student->setFirstname($dto->getFirstname());
        $student->setLastname($dto->getLastname());

        $birthDay = new DateTime($dto->getBirthday());
        $student->setBirthday($birthDay);

        $keyStage = $this->getKeyStageByBirthday($birthDay);
        $student->setKeyStage($keyStage);

        $this->studentRepo->save($student, true);

        return $student;
    }

    /**
     * @throws Exception
     */
    public function getKeyStageByBirthday(DateTime $birthday): KeyStage
    {
        $keyStageId = $this->getKeyStageIdByBirthday($birthday);

        $keyStage = $this->keyStageRepo->find($keyStageId);
        if (! $keyStage instanceof KeyStage) {
            throw new Exception('Student birthday is not valid');
        }

        return $keyStage;
    }

    /**
     * @throws Exception
     */
    public function getKeyStageIdByBirthday(DateTime $birthday): int
    {
        $currentDate = new DateTime();

        $currentYear  = $currentDate->format('Y');
        $birthdayYear = $birthday->format('Y');

        $age = $currentYear - $birthdayYear;
        if ($currentDate < $birthday->modify('+' . $age . ' years')) {
            $age--;
        }

        if ($age < 5 || $age > 16) {
            throw new Exception('Student birthday is not valid');
        }

        $matchingKeys = array_keys(array_filter($this->ageKeyStageMapping, function ($range) use ($age) {
            return $age >= $range['min'] && $age < $range['max'];
        }));

        if (empty($matchingKeys)) {
            throw new Exception('Student birthday is not valid');
        }

        return $matchingKeys[0];
    }
}
