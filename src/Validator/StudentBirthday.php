<?php

namespace App\Validator;

use Symfony\Component\Validator\Constraint;

#[\Attribute]
class StudentBirthday extends Constraint
{
    public string $invalidMessage = 'Student birthday is not valid';
    public string $notSupportedMessage = 'Student birthday is not supported';
}
