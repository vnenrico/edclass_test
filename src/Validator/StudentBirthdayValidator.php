<?php

namespace App\Validator;

use DateTime;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;
use Symfony\Component\Validator\Validation;

class StudentBirthdayValidator extends ConstraintValidator
{
    /**
     * @throws Exception
     */
    public function validate(mixed $value, Constraint $constraint): void
    {
        if (! $constraint instanceof StudentBirthday) {
            throw new UnexpectedTypeException($constraint, StudentBirthday::class);
        }

        if (!\is_string($value)) {
            throw new UnexpectedValueException($value, 'string');
        }

        $validator  = Validation::createValidator();
        $violations = $validator->validate($value, new Date());
        if (count($violations) > 0) {
            $this->context->buildViolation($constraint->invalidMessage)
                          ->setParameter('{{ value }}', $this->formatValue($value))
                          ->setCode(Date::INVALID_DATE_ERROR)
                          ->addViolation();

            return;
        }

        $currentDate = new DateTime();
        $birthday    = new DateTime($value);

        $currentYear  = $currentDate->format('Y');
        $birthdayYear = $birthday->format('Y');

        $age = $currentYear - $birthdayYear;
        if ($currentDate < $birthday->modify('+' . $age . ' years')) {
            $age--;
        }

        if ($age >= 5 && $age < 16) {
            return;
        }

        $this->context->buildViolation($constraint->notSupportedMessage)->addViolation();
    }
}
