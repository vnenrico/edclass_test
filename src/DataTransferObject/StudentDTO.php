<?php

namespace App\DataTransferObject;

use App\Validator as CustomAssert;
use Symfony\Component\Validator\Constraints as Assert;

readonly class StudentDTO
{
    public function __construct(
        #[Assert\NotBlank]
        #[Assert\Length(min: 3)]
        private string $firstname,
        #[Assert\NotBlank]
        #[Assert\Length(min: 3)]
        private string $lastname,
        #[Assert\NotBlank]
        #[CustomAssert\StudentBirthday]
        private string $birthday
    ) {
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getBirthday(): string
    {
        return $this->birthday;
    }
}
