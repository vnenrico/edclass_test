<?php

namespace App\EventListener;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class ExceptionListener
{
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        if ($exception instanceof NotFoundHttpException) {
            $response = new JsonResponse([
                'message' => 'Not Found'
            ], Response::HTTP_NOT_FOUND);
            $event->setResponse($response);

            return;
        }

        $response = new JsonResponse([
            'message' => 'Internal Server Error'
        ], Response::HTTP_INTERNAL_SERVER_ERROR);
        $event->setResponse($response);
    }
}
