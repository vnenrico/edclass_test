<?php

namespace App\Controller;

use App\DataTransferObject\StudentDTO;
use App\Entity\Student;
use App\Services\StudentService;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

#[Route(path: '/api/student')]
class StudentController extends AbstractController
{
    public function __construct(
        private readonly StudentService $studentServ,
    ) {
    }

    #[Route(path: '/{id}', name: 'get_student', requirements: ['id' => '\d+'], methods: 'GET')]
    public function getStudentAction(Student $student, SerializerInterface $serializer): JsonResponse
    {
        return JsonResponse::fromJsonString(
            $serializer->serialize($student, 'json', ['groups' => 'student-detail', 'datetime_format' => 'Y-m-d'])
        );
    }

    /**
     * @throws Exception
     */
    #[Route(name: 'create_student', methods: 'POST')]
    public function createStudentAction(
        Request $request,
        SerializerInterface $serializer,
        ValidatorInterface $validator
    ): JsonResponse {
        if ('' === $data = $request->getContent()) {
            return $this->json([
                'message' => 'Request body is empty',
            ], Response::HTTP_BAD_REQUEST);
        }

        $dto = $serializer->deserialize($data, StudentDTO::class, 'json');
        $violations = $validator->validate($dto);
        if (count($violations) > 0) {
            $errors = array_map(
                fn($violation) => [
                    'code'     => $violation->getCode(),
                    'message'  => $violation->getMessage(),
                    'property' => $violation->getPropertyPath()
                ],
                iterator_to_array($violations)
            );

            return new JsonResponse([
                'errors' => $errors
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
        }

        $student = $this->studentServ->createStudent($dto);

        return new JsonResponse(['id' => $student->getId()], Response::HTTP_CREATED);
    }
}
