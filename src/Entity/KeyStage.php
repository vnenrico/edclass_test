<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\KeyStageRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: KeyStageRepository::class)]
class KeyStage
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: 'student-detail')]
    private ?int $id = null;

    #[ORM\Column(length: 5)]
    #[Groups(groups: 'student-detail')]
    private ?string $name = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;

        return $this;
    }
}
