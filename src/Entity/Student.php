<?php

declare(strict_types=1);

namespace App\Entity;

use App\Repository\StudentRepository;
use DateTime;
use DateTimeInterface;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Attribute\Groups;

#[ORM\Entity(repositoryClass: StudentRepository::class)]
#[ORM\HasLifecycleCallbacks]
class Student
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    #[Groups(groups: 'student-detail')]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: 'student-detail')]
    private ?string $firstname = null;

    #[ORM\Column(length: 255)]
    #[Groups(groups: 'student-detail')]
    private ?string $lastname = null;

    #[ORM\Column(type: Types::DATE_MUTABLE)]
    #[Groups(groups: 'student-detail')]
    private ?DateTimeInterface $birthday = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    #[Groups(groups: 'student-detail')]
    private ?KeyStage $keyStage = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: false)]
    #[Groups(groups: 'student-detail')]
    protected ?DateTimeInterface $createdAt = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    #[Groups(groups: 'student-detail')]
    protected ?DateTimeInterface $updatedAt = null;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstname(): ?string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): static
    {
        $this->firstname = $firstname;

        return $this;
    }

    public function getLastname(): ?string
    {
        return $this->lastname;
    }

    public function setLastname(string $lastname): static
    {
        $this->lastname = $lastname;

        return $this;
    }

    public function getBirthday(): ?DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(DateTimeInterface $birthday): static
    {
        $this->birthday = $birthday;

        return $this;
    }

    public function getKeyStage(): ?KeyStage
    {
        return $this->keyStage;
    }

    public function setKeyStage(?KeyStage $keyStage): static
    {
        $this->keyStage = $keyStage;

        return $this;
    }

    public function getCreatedAt(): ?DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(?DateTimeInterface $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?DateTimeInterface $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    #[ORM\PrePersist]
    public function beforeAdd(): void
    {
        $dateTime = new DateTime();
        $this->setCreatedAt($dateTime);
    }

    #[ORM\PreUpdate]
    public function beforeEdit(): void
    {
        $dateTime = new DateTime();
        $this->setUpdatedAt($dateTime);
    }
}
