<?php

namespace App\Tests\Controller;

use App\Kernel;
use Exception;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\HttpFoundation\Response;

class StudentControllerTest extends WebTestCase
{
    private KernelBrowser $client;
    private Application $application;

    /**
     * @throws Exception
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->setupTest();
        $this->setupDatabase();
    }

    private function setupTest(): void
    {
        if (static::$kernel !== null) {
            return;
        }

        static::$kernel = new Kernel('test', true);
        static::$kernel->boot();

        $this->application = new Application(static::$kernel);
        $this->application->setAutoExit(false);

        $this->client = static::createClient();
    }

    /**
     * @throws Exception
     */
    protected function setupDatabase(): void
    {
        $this->application->run(new ArrayInput([
            'command'     => 'doctrine:database:drop',
            '--if-exists' => '1',
            '--force'     => '1',
            '--quiet'     => '1'
        ]));

        $this->application->run(new ArrayInput([
            'command' => 'doctrine:database:create',
            '--quiet' => '1'
        ]));

        $this->application->run(new ArrayInput([
            'command'          => 'doctrine:migrations:migrate',
            '--no-interaction' => '1'
        ]));
    }

    /**
     * @dataProvider dataProviderStudentCreateValid
     */
    public function testStudentCreateValid(string $content): void
    {
        $this->client->request(method: 'POST', uri: '/api/student', content: $content);
        $response = $this->client->getResponse();

        $this->assertEquals(Response::HTTP_CREATED, $response->getStatusCode());
    }

    /**
     * @return array<int, array<int, string>>
     */
    public static function dataProviderStudentCreateValid(): array
    {
        return [
            ['{"firstname": "John","lastname": "Dane","birthday": "2019-01-01"}'],
        ];
    }

    /**
     * @dataProvider dataProviderStudentCreateInvalid
     */
    public function testStudentCreateInvalid(string $content, int $statusCode): void
    {
        $this->client->request(method: 'POST', uri: '/api/student', content: $content);
        $response = $this->client->getResponse();

        $this->assertEquals($statusCode, $response->getStatusCode());
    }

    /**
     * @return array<int, array<int, int|string>>
     */
    public static function dataProviderStudentCreateInvalid(): array
    {
        return [
            ['', Response::HTTP_BAD_REQUEST],
            ['{"firstname": "Jo","lastname": "Dane","birthday": "2019-01-01"}', Response::HTTP_UNPROCESSABLE_ENTITY],
            ['{"firstname": "John","lastname": "Da","birthday": "2019-01-01"}', Response::HTTP_UNPROCESSABLE_ENTITY],
            ['{"firstname": "John","lastname": "Dane","birthday": "2024-01-01"}', Response::HTTP_UNPROCESSABLE_ENTITY],
            ['{"firstname": "John","lastname": "Dane","birthday": "2024-01-001"}', Response::HTTP_UNPROCESSABLE_ENTITY],
        ];
    }

    public function testGetStudent(): void
    {
        $this->client->request('GET', '/api/student/1');
        $response = $this->client->getResponse();
        $data     = $response->getContent();

        $this->assertIsString($data);

        $result = json_decode($data, true);

        $this->assertIsArray($result);
        $this->assertArrayHasKey('id', $result);
        $this->assertArrayHasKey('firstname', $result);
        $this->assertArrayHasKey('lastname', $result);
        $this->assertArrayHasKey('birthday', $result);
        $this->assertArrayHasKey('key_stage', $result);
        $this->assertArrayHasKey('id', $result['key_stage']);
        $this->assertArrayHasKey('name', $result['key_stage']);
    }
}
