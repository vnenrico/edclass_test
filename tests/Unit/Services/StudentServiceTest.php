<?php

namespace App\Tests\Unit\Services;

use App\Repository\KeyStageRepository;
use App\Repository\StudentRepository;
use App\Services\StudentService;
use DateTime;
use Exception;
use PHPUnit\Framework\TestCase;

class StudentServiceTest extends TestCase
{
    private StudentService $studentServ;

    protected function setUp(): void
    {
        $mockStudentRepo  = $this->createMock(StudentRepository::class);
        $mockKeyStageRepo = $this->createMock(KeyStageRepository::class);

        $ageKeyStageMapping = [
            1 => ['min' => 5, 'max' => 7],
            2 => ['min' => 7, 'max' => 11],
            3 => ['min' => 11, 'max' => 14],
            4 => ['min' => 14, 'max' => 16]
        ];

        $this->studentServ = new StudentService($ageKeyStageMapping, $mockStudentRepo, $mockKeyStageRepo);
    }


    /**
     * @throws Exception
     *
     * @dataProvider dataProviderGetKeyStageByBirthdayInvalid
     *
     */
    public function testGetKeyStageByBirthdayInvalid(string $birthday): void
    {
        $birthday = new DateTime($birthday);

        try {
            $this->studentServ->getKeyStageIdByBirthday($birthday);
        } catch (Exception $exception) {
            $this->assertEquals('Student birthday is not valid', $exception->getMessage());
        }
    }

    /**
     * @return array<int, array<int, string>>
     */
    public static function dataProviderGetKeyStageByBirthdayInvalid(): array
    {
        return [
            ['-3 years'],
            ['-5 years +1 day'],
            ['-16 years -1 day'],
            ['-20 years'],
        ];
    }

    /**
     * @throws Exception
     *
     * @dataProvider dataProviderGetKeyStageByBirthdayValid
     *
     */
    public function testGetKeyStageByBirthdayValid(string $birthday, int $id): void
    {
        $birthday = new DateTime($birthday);

        $result = $this->studentServ->getKeyStageIdByBirthday($birthday);
        $this->assertEquals($result, $id);
    }

    /**
     * @return array<int, array<int, int|string>>
     */
    public static function dataProviderGetKeyStageByBirthdayValid(): array
    {
        return [
            ['-6 years', 1],
            ['-7 years +1 day', 1],
            ['-7 years -1 day', 2],
            ['-10 years', 2],
            ['-11 years +1 day', 2],
            ['-11 years -1 day', 3],
            ['-12 years', 3],
            ['-14 years +1 day', 3],
            ['-14 years -1 day', 4],
            ['-15 years', 4],
        ];
    }
}
