<?php

namespace App\Tests\Unit\Validator;

use App\Validator\StudentBirthday as Constraint;
use DateTime;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class StudentBirthdayValidatorTest extends TestCase
{
    /**
     * @dataProvider dataProviderValid
     */
    public function testValidateValid(string $birthday): void
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($birthday, new Constraint());

        $this->assertEquals(0, count($violations));
    }

    /**
     * @return array<int, array<int, string>>
     */
    public function dataProviderValid(): array
    {
        return [
            [(new DateTime('-6 years'))->format('Y-m-d')],
            [(new DateTime('-10 years'))->format('Y-m-d')],
            [(new DateTime('-12 years'))->format('Y-m-d')],
            [(new DateTime('-15 years'))->format('Y-m-d')],
        ];
    }

    /**
     * @dataProvider dataProviderInvalid
     */
    public function testValidateInvalid(string $birthday): void
    {
        $validator = Validation::createValidator();
        $violations = $validator->validate($birthday, new Constraint());

        $this->assertNotEquals(0, count($violations));
    }

    /**
     * @return array<int, array<int, string>>
     */
    public function dataProviderInvalid(): array
    {
        return [
            [(new DateTime('-3 years'))->format('Y-m-d')],
            [(new DateTime('-5 years +1 day'))->format('Y-m-d')],
            [(new DateTime('-16 years -1 day'))->format('Y-m-d')],
            [(new DateTime('-20 years'))->format('Y-m-d')],
            ['test']
        ];
    }
}
